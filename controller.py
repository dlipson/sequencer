import socket, os, time, pygame

s = socket.socket()
s.connect(('localhost', 3000))
pygame.init()
pygame.display.set_mode((10,10))

def send(route, value):
	s.sendall(str(route) + " " + str(value) + ";")

while True:
	pygame.event.pump()
	keys = pygame.key.get_pressed()

	## drum notes
	if keys[ord('`')]:
		send('toggle_beat', 0)
	if keys[ord('1')]:
		send('toggle_beat', 1)
	if keys[ord('2')]:
		send('toggle_beat', 2)
	if keys[ord('3')]:
		send('toggle_beat', 3)
	if keys[ord('4')]:
		send('toggle_beat', 4)
	if keys[ord('5')]:
		send('toggle_beat', 5)
	if keys[ord('6')]:
		send('toggle_beat', 6)
	if keys[ord('7')]:
		send('toggle_beat', 7)

	## drum mode
	if keys[ord('8')]:
		send('mode', 0)
	if keys[ord('9')]:
		send('mode', 1)
	if keys[ord('0')]:
		send('mode', 2)

	## bpm
	if keys[ord('=')]:
		send('bp', 10)
	elif keys[ord('-')]:
		send('bp', -10)

		## pause/play
	if keys[pygame.K_TAB]:
		send('pl', 1)

	## synth mode
	if keys[ord('q')]:
		send('play_synth', 1)

	## synth keys
	if keys[ord('q')]:
		send('play_synth', 1)
	if keys[ord('w')]:
		send('play_synth', 2)
	if keys[ord('e')]:
		send('play_synth', 3)
	if keys[ord('r')]:
		send('play_synth', 4)
	if keys[ord('t')]:
		send('play_synth', 5)
	if keys[ord('y')]:
		send('play_synth', 6)
	if keys[ord('u')]:
		send('play_synth', 7)
	if keys[ord('i')]:
		send('play_synth', 8)
	if keys[ord('o')]:
		send('play_synth', 9)
	if keys[ord('p')]:
		send('play_synth', 10)
	if keys[ord('[')]:
		send('play_synth', 11)
	if keys[ord(']')]:
		send('play_synth', 12)
	## mute note
	if keys[ord('\\')]:
		send('play_synth', 0)

	## synth voice
	if keys[ord('a')]:
		send('synth_voice', 0)
	if keys[ord('s')]:
		send('synth_voice', 1)
	if keys[ord('d')]:
		send('synth_voice', 2)

	## play mode vs record mode (in play mode, just plays note or beat), in record mode, tabwrites
	if keys[ord(';')]:
		send('recording', 1)
	if keys[ord('\'')]:
		send('recording', 0)

	## mute current drum
	if keys[ord('m')]:
		send('mute', 1)

	if keys[ord(',')]:
		send('setoctave', 0)
	if keys[ord('.')]:
		send('setoctave', 1)
	if keys[ord('/')]:
		send('setoctave', 2)

	## clear sequences
	if keys[ord('x')]:
		send('clearall', 1)

	time.sleep(0.1)